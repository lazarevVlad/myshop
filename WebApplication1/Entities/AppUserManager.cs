﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Entities;
using WebApplication1.Services;

namespace WebApplication1.Entities
{
    public class AppUserManager : UserManager<AppUser>
    {
        public AppUserManager(IUserStore<AppUser> store) : base(store)
        {
        }
        public static AppUserManager Create(IdentityFactoryOptions<AppUserManager> otions, IOwinContext context)
        {
            AppContext db = context.Get<AppContext>();
            AppUserManager manager = new AppUserManager(new UserStore<AppUser>(db));
            manager.UserTokenProvider = new DataProtectorTokenProvider<AppUser>(new DpapiDataProtectionProvider("qweqwe").Create("EmailConfirmation"));
            manager.EmailService = new EmailService();
            return manager;
        }
    }
}