namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class asd : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ParameterValues", "GameId", "dbo.Games");
            DropIndex("dbo.ParameterValues", new[] { "GameId" });
            CreateTable(
                "dbo.ParameterValueGames",
                c => new
                    {
                        ParameterValue_ParameterValueId = c.Int(nullable: false),
                        Game_GameId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ParameterValue_ParameterValueId, t.Game_GameId })
                .ForeignKey("dbo.ParameterValues", t => t.ParameterValue_ParameterValueId, cascadeDelete: true)
                .ForeignKey("dbo.Games", t => t.Game_GameId, cascadeDelete: true)
                .Index(t => t.ParameterValue_ParameterValueId)
                .Index(t => t.Game_GameId);
            
            DropColumn("dbo.ParameterValues", "GameId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ParameterValues", "GameId", c => c.Int(nullable: false));
            DropForeignKey("dbo.ParameterValueGames", "Game_GameId", "dbo.Games");
            DropForeignKey("dbo.ParameterValueGames", "ParameterValue_ParameterValueId", "dbo.ParameterValues");
            DropIndex("dbo.ParameterValueGames", new[] { "Game_GameId" });
            DropIndex("dbo.ParameterValueGames", new[] { "ParameterValue_ParameterValueId" });
            DropTable("dbo.ParameterValueGames");
            CreateIndex("dbo.ParameterValues", "GameId");
            AddForeignKey("dbo.ParameterValues", "GameId", "dbo.Games", "GameId", cascadeDelete: true);
        }
    }
}
