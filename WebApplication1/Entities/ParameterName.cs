﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Entities
{
    public class ParameterName
    {
        public int ParameterNameId { get; set; }
        public string Name { get; set; }
    }
    public class ParameterValue
    {
        public int ParameterValueId { get; set; }
        public string Name { get; set; }

        public ParameterName ParameterName { get; set; }
        public int ParameterNameId { get; set; }

        public List<Game> Games { get; set; }

    }
}