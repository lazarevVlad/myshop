﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Entities;

namespace WebApplication1.Models
{
    public class GameViewModel
    {
        public IEnumerable<Game> Games { get; set; }
        public int CurrentPage { get; set; }
        public string CurrentGenre { get; set; }
        public int TotalPage { get; set; }
    }
}