﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication1.Entities
{
    public class AppContext : IdentityDbContext<AppUser>
    {
        public AppContext() : base("IdentityDb")
        {
        }
        public static AppContext Create()
        {
            return new AppContext();
        }
        public DbSet<Game> Games { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<CartLine> CartLines { get; set; }
        public DbSet<ParameterName> ParameterNames { get; set; }
        public DbSet<ParameterValue> ParameterValues { get; set; }
    }
}