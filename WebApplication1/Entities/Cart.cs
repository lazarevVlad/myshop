﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Entities
{
    public class Cart
    {
        public int CartId { get; set; }
        public List<CartLine> CartLines { get; set; }
        public string UserId { get; set; }
        public AppUser User { get; set; }
        public Cart()
        {
            CartLines = new List<CartLine>();
        }

    }

    public class CartLine
    {
        public int CartLineId { get; set; }
        public int GameId { get; set; }
        public Game Game { get; set; }
        public int CartId { get; set; }
        public Cart Cart { get; set; }
        public int Count { get; set; }
    }
}