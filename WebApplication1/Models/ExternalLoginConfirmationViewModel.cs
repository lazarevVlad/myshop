﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Display(Name ="Email")]
        [Required]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Некорректный адрес")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Возраст")]
        public int? Year { get; set; }
        public string LoginProvider { get; set; }
        public string ReturnUrl { get; set; }
    }
}