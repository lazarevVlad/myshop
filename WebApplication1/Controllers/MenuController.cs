﻿using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Entities;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class MenuController : Controller
    {
        private AppContext Context
        {
            get
            {
                return HttpContext.GetOwinContext().Get<AppContext>();
            }
        }
        public PartialViewResult Index(string genre)
        {
            MenuViewModel model = new MenuViewModel();
            var parametersNames = Context.ParameterNames.Select(m=>m.Name).ToList();
            foreach (var item in parametersNames)
            {
                model.Parameters.Add(new ParameterGame
                {
                    Name = item,
                    Values = Context.ParameterValues.Include(m => m.ParameterName).Where(n => n.ParameterName.Name == item).Select(b => b.Name).ToArray()
                });
            }
            model.Select = genre;
            return PartialView(model);
        }
    }
}