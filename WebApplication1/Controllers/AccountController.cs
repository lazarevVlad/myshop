﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using WebApplication1.Models;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Text;

namespace WebApplication1.Controllers
{

    public class AccountController : Controller
    {
        private AppUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        private AppSignInManager SignInManager
        {
            get
            {
                return HttpContext.GetOwinContext().Get<AppSignInManager>();
            }
        }
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }
        public PartialViewResult LoginInfo()
        {
            return PartialView("_LoginInfo", HttpContext.User);
        }

        [Route("Account/Index")]
        [Route("Account/Login")]
        [Route("Account/Register")]
        public ActionResult Index(string returnUrl)
        {
            return View(new IndexViewModel { ReturnUrl = returnUrl });
        }
        [HttpPost]
        public string LoginAjax(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                AppUser user = UserManager.Find(model.Login, model.Password);
                if (user == null)
                {
                    ModelState.AddModelError("", "Неверные логин и пароль");
                    return CreateErrorMesssage(ModelState);
                }
                if (UserManager.IsEmailConfirmed(user.Id))
                {
                    ClaimsIdentity claim = UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = true }, claim);
                    return "successful";
                }
                else
                {
                    string link = "<a id=\"resendEmail\" href=\"\\Account\\SendEmailConfirmationToken?userId=" + user.Id + "\">Отправтить письмо повторно</a><script>ResendEmail()</script>";
                    ModelState.AddModelError("", "Email не подтвержден. " + link);
                    return CreateErrorMesssage(ModelState);
                }
            }
            return CreateErrorMesssage(ModelState);
        }
        [HttpPost]
        public string RegisterAjax(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                AppUser user = new AppUser { Email = model.Email, UserName = model.Email, Year = (int)model.Year };
                IdentityResult result = UserManager.Create(user, model.Password);
                if (result.Succeeded)
                {
                    SendEmailConfirmationToken(user.Id);
                    UserManager.AddToRole(UserManager.FindByName(user.UserName).Id, "User");
                    return "successful";
                }
                else
                {
                    foreach (var item in result.Errors)
                    {
                        ModelState.AddModelError("", item);
                    }
                    return CreateErrorMesssage(ModelState);
                }
            }
            return CreateErrorMesssage(ModelState); ;
        }
        private string CreateErrorMesssage(ModelStateDictionary modelState)
        {
            StringBuilder errors = new StringBuilder();
            foreach (var item in modelState.Values)
            {
                foreach (var error in item.Errors)
                {
                    errors.AppendLine("<p>");
                    errors.AppendLine(error.ErrorMessage.ToString());
                    errors.AppendLine("<p>");
                }
            }

            return errors.ToString();
        }
        public ActionResult ConfirmEmail(string userId, string code)
        {

            var result = UserManager.ConfirmEmail(userId, HttpUtility.UrlDecode(code));
            if (result.Succeeded)
            {
                TempData["message"] = "Email подтвержден, можете залогиниться";
            }
            else
            {
                string errors = "";
                foreach (var item in result.Errors)
                {
                    errors += " " + item.ToString();
                }
                TempData["message"] = errors;
            }
            return RedirectToAction("Index", "Account");
        }
        public void SendEmailConfirmationToken(string userId)
        {
            var code = UserManager.GenerateEmailConfirmationToken(userId);
            code = System.Web.HttpUtility.UrlEncode(code);
            var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = userId, code = code },
               protocol: Request.Url.Scheme);
            UserManager.SendEmail(userId, "Подтверждение электронной почты",
                "Для завершения регистрации перейдите по ссылке: <a href=\""
                + callbackUrl + "\">завершить регистрацию</a>");
        }
        public string ForgotPasswordAjax(LoginViewModel model)
        {
            if (model.Login == null)
            {
                return "Укажите адрес электронной почты";
            }
            AppUser user = UserManager.FindByEmail(model.Login);
            if (user != null)
            {
                string code = UserManager.GeneratePasswordResetToken(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = HttpUtility.UrlEncode(code) }, protocol: Request.Url.Scheme);
                UserManager.SendEmail(user.Id, "Сброс пароля", "Для сброса пароля, перейдите по ссылке <a href=\"" + callbackUrl + "\">сбросить</a>");
                return "<div>Письмо с инструкиями для сброса пароля отправлено на указанную почту</div>";
            }
            return "Пользователь с указанной электронной почтой не зарегистрирован";
        }
        [HttpGet]
        public ActionResult ResetPassword(string userId, string code)
        {
            return View(new ForgotPasswordViewModel { UserId = userId, Code = code });
        }
        [HttpPost]
        public ActionResult ResetPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = UserManager.ResetPassword(model.UserId, HttpUtility.UrlDecode(model.Code), model.Password);
                if (result.Succeeded)
                {
                    TempData["message"] = "Пароль изменен";
                    return RedirectToAction("Index", "Account");
                }
                else
                {
                    foreach (var item in result.Errors)
                    {
                        ModelState.AddModelError("", item);
                        return View(model);
                    }
                }
            }
            return View(model);
        }
        public void ExternalLogin(string provider, string returnUrl)
        {
            var RedirectUri = Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl });
            var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
            HttpContext.GetOwinContext().Authentication.Challenge(properties, provider);
        }
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = AuthenticationManager.GetExternalLoginInfo();
            if (loginInfo == null)
            {
                return RedirectToAction("Index","Account");
            }
            var result = SignInManager.ExternalSignIn(loginInfo, isPersistent: false);

            if (result==SignInStatus.Failure)
            {
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email, LoginProvider = loginInfo.Login.LoginProvider, ReturnUrl = returnUrl });
            }
            if (result == SignInStatus.Success)
            {
                return RedirectToAction("Index","Home");
            }
            return null;
        }
        public ActionResult ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Получение сведений о пользователе от внешнего поставщика входа
                var info =  AuthenticationManager.GetExternalLoginInfo();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                AppUser user = new AppUser { UserName = model.Email, Email = model.Email, Year = (int)model.Year, EmailConfirmed = true };
                IdentityResult result = UserManager.Create(user);
                if (result.Succeeded)
                {
                    result = UserManager.AddLogin(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        SignInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                        return Redirect(model.ReturnUrl??"/Home/Index");
                    }
                }
                foreach (var item in result.Errors)
                {
                    ModelState.AddModelError("", item);
                }
            }
            return View(model);
        }
    }
}