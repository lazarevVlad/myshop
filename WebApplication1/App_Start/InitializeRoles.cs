﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApplication1.Entities;

namespace WebApplication1.App_Start
{
    
    public static class Initialize
    {
        public static void FirstInitializeDb()
        {
            AppContext context = AppContext.Create();
            if (context.Roles.ToList().Count==0)
            {
                AppRoleManager roleManager = new AppRoleManager(new RoleStore<AppRole>(context));
                AppUserManager userManager = new AppUserManager(new UserStore<AppUser>(context));
                AppRole role1 = new AppRole
                {
                    Id = "1",
                    Name = "Admin",
                };
                AppRole role2 = new AppRole
                {
                    Id = "2",
                    Name = "User",
                };
                roleManager.Create(role1);
                roleManager.Create(role2);
                userManager.Create(new AppUser
                {
                    UserName = "admin",
                    Email = "admin@admin"
                }, "111111");
                userManager.AddToRole(userManager.FindByName("admin").Id, role1.Name); 
            }
        }
    }
}