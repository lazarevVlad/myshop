﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Entities;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    { 
        private AppUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }
        public ActionResult Index()
        {
            return View();
        }
        [Authorize(Roles ="User")]
        public ActionResult Index2()
        {
            return View("Index");
        }
    }
}