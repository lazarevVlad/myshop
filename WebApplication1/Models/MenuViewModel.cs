﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class MenuViewModel
    {
        public string Select { get; set; }
        public List<ParameterGame> Parameters { get; set; }
        public MenuViewModel()
        {
            Parameters = new List<ParameterGame>();
        }
    }

    public class ParameterGame
    {
        public string Name { get; set; }
        public IEnumerable<string> Values { get; set; }
    }
}