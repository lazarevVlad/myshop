﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Entities
{
    public class Game
    {
        public int GameId { get; set; }
        public string Name { get; set; }
        public string Discription { get; set; }
        public string Cover { get; set; }
        public decimal Price { get; set; }
        public List<ParameterValue> ParameterValues { get; set; }

    }
}