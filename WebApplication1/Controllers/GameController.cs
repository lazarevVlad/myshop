﻿using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Entities;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using WebApplication1.Models;
using System;

namespace WebApplication1.Controllers
{
    public class GameController : Controller
    {
        private int _itemPerPage = 6;
        private AppContext Context
        {
            get
            {
                return HttpContext.GetOwinContext().Get<AppContext>();
            }
        }
        // GET: Game
        public ActionResult Index(string genre, int page = 1)
        {
            var games = Context.Games.Include(m => m.ParameterValues.Select(n => n.ParameterName));
            //.Where(m => m.ParameterValues)
            GameViewModel result = new GameViewModel();
            result.Games = Context.Games
                //.Where(m => m.Genre == (genre??m.Genre))
                .OrderBy(m => m.GameId)
                .Skip((page - 1) * (_itemPerPage))
                .Take(_itemPerPage);
            result.CurrentPage = page;
            result.CurrentGenre = genre;
            result.TotalPage = games.Count() % _itemPerPage == 0 ? games.Count() / _itemPerPage : games.Count() / _itemPerPage + 1;
            return View(result);
        }
        public ActionResult IndexAjax(List<FilterData> result, int page=1)
        {
            List<Game> queryResult = Context.Games.ToList();
            if (result != null)
            {
                var qwe = new List<FilterData>();
                qwe.Add(new FilterData { Name = "genre", Value = "Шутер" });
                qwe.Add(new FilterData { Name = "operatingSystem", Value = "Mac OS X" });
                qwe.Add(new FilterData { Name = "genre", Value = "Экшен" });
                var groupResult = result.GroupBy(m => m.Name, m => m.Value);
                foreach (var name in groupResult)
                {
                    List<Game> resultByPropertyName = new List<Game>(); ;
                    foreach (var value in name)
                    {
                        var tempResult = Context.ParameterValues.Where(m => m.Name == value).SelectMany(n => n.Games).ToList();
                        resultByPropertyName = resultByPropertyName.Union(tempResult).ToList();
                    }
                    queryResult = queryResult.Intersect(resultByPropertyName).ToList();
                }
            }

            GameViewModel resultToView = new GameViewModel();
            resultToView.Games = queryResult
                .OrderBy(m => m.GameId)
                .Skip((page - 1) * (_itemPerPage))
                .Take(_itemPerPage);
            resultToView.CurrentPage = page;
            resultToView.CurrentGenre = "";
            resultToView.TotalPage = queryResult.Count() % _itemPerPage == 0 ? queryResult.Count() / _itemPerPage : queryResult.Count() / _itemPerPage + 1;
            return PartialView("Index", resultToView);
        }
    }
}