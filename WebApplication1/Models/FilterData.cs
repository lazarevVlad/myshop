﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class FilterData
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}