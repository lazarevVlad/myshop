﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Entities;
using System.Data.Entity;

namespace WebApplication1.Controllers
{
    [Authorize(Roles ="User, Admin")]
    public class CartController : Controller
    {
        private AppUser CurrentUser
        {
            get
            {
               return UserManager.FindByEmail(HttpContext.User.Identity.Name);
            }
        }
        private AppUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }
        private AppContext Context
        {
            get
            {
                return HttpContext.GetOwinContext().Get<AppContext>();
            }
        }
        // GET: Cart
        public void AddToCart(int gameId)
        {
            AppUser user = CurrentUser;
            Cart cart = Context.Carts.Include(n=>n.CartLines).FirstOrDefault(m => m.UserId == user.Id);
            if (cart==null)
            {
                cart = new Cart
                {
                    UserId = user.Id
                };
                Context.Carts.Add(cart);
            }
            CartLine cartLine = cart.CartLines.FirstOrDefault(m => m.GameId == gameId&&m.CartId==cart.CartId);
            if (cartLine==null)
            {
                cartLine = new CartLine
                {
                    CartId = cart.CartId,
                    GameId = gameId,
                    Count = 1
                };
                cart.CartLines.Add(cartLine);
            }
            else
            {
                cartLine.Count++;
            }
            Context.SaveChanges();
        }
        public ActionResult Index()
        {
            AppUser user = CurrentUser;
            var games = Context.Games.ToList();
            Cart cart = Context.Carts.Include(x=>x.CartLines.Select(p=>p.Game)).FirstOrDefault(m => m.UserId == user.Id);


            return View(cart);
        }
        public PartialViewResult CartInfo()
        {
            return PartialView();
        }

    }
}